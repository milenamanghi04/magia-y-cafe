using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMov8Direccion : MonoBehaviour
{
    //ACTUALMENTE SOLO 4
    [SerializeField] private float speed = 3f;

    private Rigidbody rb;
    private Vector3 moveInput;
    private Animator playerAnimator;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        playerAnimator = GetComponent<Animator>();
    }

    private void Update()
    {
        float moveX = Input.GetAxisRaw("Horizontal");
        float moveZ = Input.GetAxisRaw("Vertical");
        moveInput = new Vector3(moveX, 0, moveZ).normalized;

        playerAnimator.SetFloat("Horizontal", moveX);
        playerAnimator.SetFloat("Vertical", moveZ);
        playerAnimator.SetFloat("Speed", moveInput.sqrMagnitude);

    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + moveInput * speed * Time.fixedDeltaTime);
    }
}
